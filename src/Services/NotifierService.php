<?php

namespace App\Services;

use Twig\Environment;
use App\Document\User;
use Exception;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class NotifierService
{
    private MailerInterface $mailer;

    public function __construct(
        MailerInterface $mailer

    )
    {
        $this->mailer = $mailer;
    }


    public function notify(User $user)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@testNoitfication.de')
            ->to(new Address($user->getEmail(),$user->getName()))
            ->subject('test Notification')
            ->htmlTemplate('emailTemplate/Notification.html.twig')
            ->context(['msg_content'=>"Account created"])
        ;
        sleep(120);
        $this->mailer->send($email);
        
    }

}
