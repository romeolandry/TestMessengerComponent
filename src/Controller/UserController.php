<?php

namespace App\Controller;

use App\Document\User;
use App\Messages\UserNotificationMessage;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    #[Route('/user', name: 'app_user', methods: 'get')]
    public function index(): JsonResponse
    {
        /* return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]); */

        return $this->json(["name"=>"romeo"],200);
    }

    #[Route('/user/create', name: 'app_user_create', methods: 'post')]
    public function create(
        Request $request,
        MessageBusInterface $messageBus,
        DocumentManager $dm): JsonResponse
    {
        
        $parameters = json_decode($request->getContent(), true);

        // create user 
        $user = (new User())
            ->setEmail($parameters['email'])
            ->setName($parameters['name'])
        ;            

        // save
        $dm->persist($user);
        $dm->flush();

        // send Message via Bus

        $messageBus->dispatch(new UserNotificationMessage($user->getId()));

        return $this->json($user,200);
    }
}
