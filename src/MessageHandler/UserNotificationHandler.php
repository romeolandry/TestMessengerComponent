<?php

namespace App\MessageHandler;

use App\Document\User;
use App\Services\NotifierService;
use App\Messages\UserNotificationMessage;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserNotificationHandler implements MessageHandlerInterface
{
    public function __construct(
        DocumentManager $dm,
        NotifierService $service)
    {
        $this->dm = $dm;
        $this->service = $service;
    }

    public function __invoke(UserNotificationMessage $message)
    {
        $user = $this->dm->getRepository(User::class)->findOneBy(['id'=>$message->getUserId()]);

        $this->service->notify($user);
    }
}
