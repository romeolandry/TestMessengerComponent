<?php

namespace App\Messages;

class UserNotificationMessage
{
    private mixed $userId;

    public function __construct(mixed $userId)
    {
        $this->userId = $userId;
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId(): mixed
    {
        return $this->userId;
    }
}
